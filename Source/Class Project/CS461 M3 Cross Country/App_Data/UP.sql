CREATE TABLE dbo.Athletes
(
	AthleteID			INT IDENTITY(1,1) NOT NULL,
	FirstName			NVARCHAR(50) NOT NULL,
	LastName			NVARCHAR(50) NOT NULL,
	Age					INT NOT NULL,
	Height				NVARCHAR(10) NOT NULL,
	Weight				NVARCHAR(10) NOT NULL,
	CONSTRAINT[PK_dbo.Athletes] PRIMARY KEY CLUSTERED (AthleteID ASC)
);

CREATE TABLE dbo.Parents
(
	ParentID		INT IDENTITY(1,1) NOT NULL,
	FirstName			NVARCHAR(50) NOT NULL,
	LastName			NVARCHAR(50) NOT NULL,
	CONSTRAINT[PK_dbo.Parents] PRIMARY KEY CLUSTERED (ParentID ASC)
);

CREATE TABLE dbo.Sessions
(
	TrainingID			INT IDENTITY(1,1) NOT NULL,
	TrainingDate		DATE NOT NULL,
	TrainingTime		TIME NOT NULL,
	HeartRate			INT NOT NULL,
	GPSData				TEXT NOT NULL,
	Distance			FLOAT NOT NULL,
	AthleteID			INT NOT NULL,
	CONSTRAINT[PK_dbo.TRAININGS] PRIMARY KEY CLUSTERED (TrainingID ASC),
	CONSTRAINT[FK_dbo.ATHLETES_TRAININGS] FOREIGN KEY (AthleteID)
		REFERENCES dbo.ATHLETES (AthleteID)
		ON DELETE CASCADE
);

CREATE TABLE dbo.ParentsAthletes
(
	ParentAthleteID		INT IDENTITY(1,1) NOT NULL,
	AthleteID			INT NOT NULL,
	ParentID			INT NOT NULL,
	CONSTRAINT[PK_dbo.ParentsAthletes] PRIMARY KEY CLUSTERED (ParentAthleteID ASC),
	CONSTRAINT[FK_dbo.Athletes_ParentsAthletes] FOREIGN KEY (AthleteID)
		REFERENCES dbo.Athletes (AthleteID)
		ON DELETE CASCADE,
	CONSTRAINT[FK_dbo.Parents_ParentsAthletes] FOREIGN KEY (ParentID)
		REFERENCES dbo.Parents (ParentID)
		ON DELETE CASCADE
);

CREATE TABLE dbo.Devices
(
	DeviceID			INT IDENTITY(1,1) NOT NULL,
	Description			NVARCHAR(100) NOT NULL,
	SerialNumber		NVARCHAR(20) NOT NULL,
	Manufacturer		NVARCHAR(35) NOT NULL,
	PurchaseDate		DATETIME NOT NULL,
	Damaged             BIT NOT NULL,           
	AssignedTo			INT,
	CONSTRAINT[PK_dbo.Devices] PRIMARY KEY CLUSTERED (DeviceId ASC),
	CONSTRAINT[FK_dbo.Athletes] FOREIGN KEY (AssignedTo)
		REFERENCES dbo.Athletes (AthleteID)
		ON DELETE SET NULL
);

CREATE TABLE dbo.Coaches
(
	CoachID				INT IDENTITY(1,1) NOT NULL,
	FirstName			NVARCHAR(50) NOT NULL,
	LastName			NVARCHAR(50) NOT NULL
	CONSTRAINT[PK_dbo.Coaches] PRIMARY KEY CLUSTERED (CoachID ASC)
);

CREATE TABLE dbo.Admins
(
	AdminID				INT IDENTITY(1,1) NOT NULL,
	FirstName			NVARCHAR(50) NOT NULL,
	LastName			NVARCHAR(50) NOT NULL
	CONSTRAINT[PK_dbo.Admins] PRIMARY KEY CLUSTERED (AdminID ASC)
);

INSERT INTO dbo.Athletes (FirstName, LastName, Age, Height, Weight) VALUES
('Aiden', 'Bower', '17', '5-11', '148'),
('Wayne', 'Cooper', '17', '5-08', '174'),
('Jillian', 'Matthews', '16', '5-05', '117'),
('Zimari', 'Nabar', '17', '5-09', '142'),
('Jake', 'Bower', '15', '5-7', '130')
;

INSERT INTO dbo.Parents (FirstName, LastName) VALUES
('John', 'Bower'), /* ID: 1 */
('Sarah', 'Bower'), /* ID: 2 */
('Alice', 'Cooper'), /* ID: 3 */
('Martha', 'Matthews'), /* ID: 4 */
('Abdul', 'Nabar') /* ID: 5 */
;

INSERT INTO dbo.ParentsAthletes (AthleteID, ParentID) VALUES
(1, 1),
(1, 2),
(2, 3),
(3, 4),
(4, 5),
(5, 1),
(5, 2)
;

INSERT INTO dbo.Sessions (AthleteID, TrainingDate, TrainingTime, HeartRate, GPSData, Distance) VALUES
('1', '2018-01-17', '15:20:35', 127, 'TEST DATA', '1.37'),
('1', '2018-01-18', '15:42:27', 132, 'TEST DATA', '1.74'),
('1', '2018-01-19', '16:17:55', 117, 'TEST DATA', '1.02'),
('2', '2018-01-17', '15:21:04', 101, 'TEST DATA', '1.36'),
('2', '2018-01-18', '15:40:32', 123, 'TEST DATA', '1.78'),
('2', '2018-01-19', '16:18:45', 105, 'TEST DATA', '0.98'),
('3', '2018-01-17', '16:21:04', 117, 'TEST DATA', '1.87'),
('3', '2018-01-18', '16:40:32', 142, 'TEST DATA', '2.29'),
('3', '2018-01-19', '17:18:45', 180, 'TEST DATA', '2.37'),
('4', '2018-01-17', '14:58:32', 134, 'TEST DATA', '0.79'),
('4', '2018-01-18', '16:08:04', 126, 'TEST DATA', '0.92'),
('4', '2018-01-19', '15:51:19', 134, 'TEST DATA', '0.74')
;

INSERT INTO dbo.Devices (Description, SerialNumber, Manufacturer, PurchaseDate, Damaged) VALUES
('Watch', '101', 'Garmin', '2018-01-05', 0),
('Watch', '102', 'Garmin', '2018-01-05', 0),
('Watch', '103', 'Garmin', '2018-01-05', 0),
('Watch', '104', 'Garmin', '2018-01-05', 0),
('Chest Strap', '201', 'Garmin', '2018-01-05', 0),
('Chest Strap', '202', 'Garmin', '2018-01-05', 0),
('Chest Strap', '203', 'Garmin', '2018-01-05', 0),
('Chest Strap', '204', 'Garmin', '2018-01-05', 0),
('Foot Pad', '301', 'Garmin', '2018-01-05', 0),
('Foot Pad', '302', 'Garmin', '2018-01-05', 0),
('Foot Pad', '303', 'Garmin', '2018-01-05', 0),
('Foot Pad', '304', 'Garmin', '2018-01-05', 0)
;

INSERT INTO dbo.Coaches (FirstName, LastName) VALUES
('Joe', 'Montana')
;

INSERT INTO dbo.Admins (FirstName, LastName) VALUES
('Elmo', 'Because')
;

GO