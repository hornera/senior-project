﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CS461_M3_Cross_Country.Models
{
    /// <summary>
    /// The speed that the athlete ran during the training session.
    /// </summary>
    public class Speed
    {
        // The date of the session
        public string Date { get; set; }
        // The distance traveled.
        public double Distance {get; set;}
        // The time elapsed.
        public TimeSpan Time { get; set; }
        // the speed vector based on the Distance and Time.
        // converts the stored Distance in miles to meters so speed can be
        // returned in meters/second.
        public double AverageSpeed => Math.Round((Distance*(1609.344) / Time.TotalSeconds), 2);
    }
}