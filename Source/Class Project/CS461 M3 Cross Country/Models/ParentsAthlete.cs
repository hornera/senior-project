namespace CS461_M3_Cross_Country.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ParentsAthlete
    {
        [Key]
        public int ParentAthleteID { get; set; }

        public int AthleteID { get; set; }

        public int ParentID { get; set; }

        public virtual Athlete Athlete { get; set; }

        public virtual Parent Parent { get; set; }
    }
}
