namespace CS461_M3_Cross_Country.DAL
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class XCDatabaseContext : DbContext
    {
        public XCDatabaseContext()
            : base("name=XCDatabaseContext")
        {
        }

        public virtual DbSet<Admin> Admins { get; set; }
        public virtual DbSet<Athlete> Athletes { get; set; }
        public virtual DbSet<Coach> Coaches { get; set; }
        public virtual DbSet<Device> Devices { get; set; }
        public virtual DbSet<Parent> Parents { get; set; }
        public virtual DbSet<ParentsAthlete> ParentsAthletes { get; set; }
        public virtual DbSet<Session> Sessions { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Athlete>()
                .HasMany(e => e.Devices)
                .WithOptional(e => e.Athlete)
                .HasForeignKey(e => e.AssignedTo);

            modelBuilder.Entity<Athlete>()
                .HasMany(e => e.ParentsAthletes)
                .WithRequired(e => e.Athlete)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Athlete>()
                .HasMany(e => e.Sessions)
                .WithRequired(e => e.Athlete)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Parent>()
                .HasMany(e => e.ParentsAthletes)
                .WithRequired(e => e.Parent)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Session>()
                .Property(e => e.GPSData)
                .IsUnicode(false);
        }
    }
}
