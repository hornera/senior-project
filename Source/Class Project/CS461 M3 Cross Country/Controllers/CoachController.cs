﻿using CS461_M3_Cross_Country.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace CS461_M3_Cross_Country.Controllers
{
    /// <summary>
    /// Controller for actions for Coaches.
    /// </summary>
    public class CoachController : Controller
    {
        private XCDatabaseContext db = new XCDatabaseContext();
        /// <summary>
        /// Provides the default view for the coaches.
        /// </summary>
        /// <returns>the index view.</returns>
        public ActionResult Index()
        {
            return View();
        }
    }
}