﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Remi.Models
{
    /// <summary>
    /// Class for calendar events to be passed back to Remi.
    /// </summary>
    public class CalendarEvent
    {
        public string EventName { get; set; }
        public string EventDate { get; set; }
        public string EventStartTime { get; set; }
        public string EventEndTime { get; set; }
        
    }
}