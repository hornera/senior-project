namespace Remi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Profile
    {
        public int ProfileID { get; set; }

        [Required]
        [StringLength(50)]
        public string Alias { get; set; }

        [StringLength(50)]
        public string VoicePref { get; set; }

        [Required]
        [StringLength(50)]
        public string HomeLoc { get; set; }

        [Required]
        [StringLength(50)]
        public string HomeCity { get; set; }

        [Required]
        [StringLength(50)]
        public string HomeState { get; set; }

        [Required]
        [StringLength(1024)]
        public string CalendarID { get; set; }

        public int UserID { get; set; }
    }
}
