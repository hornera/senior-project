﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/*******************************************************************************\
* Google Calendar Event Class
* Created by Devon Smith
* Western Oregon University Winter 2018
* Contributed to by: <none>
* 
* Created using tool: http://json2csharp.com
* 
* This class provides a data type to deserialize JSON data from Google Calendar
* Events into. This adds flexibility when processing data back from Google
* Calendar.
* 
\******************************************************************************/

namespace Remi.Models
{
    public class GoogleEventsResponse
    {
        /// <summary>
        /// The Google calendar event creator.
        /// </summary>
        public class GoogleEventCreator
        {
            // The email address of the event creator.
            public string email { get; set; }
            public bool self { get; set; }
        }

        /// <summary>
        /// The Google Calendar event organizer
        /// </summary>
        public class GoogleEventOrganizer
        {
            // The email address of the event organizer.
            public string email { get; set; }
            public bool self { get; set; }
        }

        /// <summary>
        /// The start time of the event.
        /// </summary>
        public class GoogleEventStart
        {
            // The Date and Time of the event.
            public DateTime dateTime { get; set; }
        }

        /// <summary>
        /// The end time of the event.
        /// </summary>
        public class GoogleEventEnd
        {
            // The ending date and time of the event.
            public DateTime dateTime { get; set; }
        }

        /// <summary>
        /// Google calendar event item.
        /// </summary>
        public class GoogleEventItem
        {
            public string kind { get; set; }
            public string etag { get; set; }
            public string id { get; set; }
            public string status { get; set; }
            public string htmlLink { get; set; }
            public DateTime created { get; set; }
            public DateTime updated { get; set; }
            public string summary { get; set; }
            public GoogleEventCreator creator { get; set; }
            public GoogleEventOrganizer organizer { get; set; }
            public GoogleEventStart start { get; set; }
            public GoogleEventEnd end { get; set; }
            public string iCalUID { get; set; }
            public int sequence { get; set; }
        }

        /// <summary>
        /// Root Object for the Google Calendar Event.
        /// </summary>
        public class GoogleEventsRootObject
        {
            public string kind { get; set; }
            public string etag { get; set; }
            public string summary { get; set; }
            public string description { get; set; }
            public DateTime updated { get; set; }
            public string timeZone { get; set; }
            public string accessRole { get; set; }
            public List<object> defaultReminders { get; set; }
            public string nextSyncToken { get; set; }
            public List<GoogleEventItem> items { get; set; }
        }
    }
}