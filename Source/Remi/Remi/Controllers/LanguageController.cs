﻿using System.IO;
using System.Net;
using System.Web.Mvc;
using static Remi.Models.LUISResponse;

namespace Remi.Controllers
{
    //Controller to handle Language Processing actions/requests
    public class LanguageController : Controller
    {

        public JsonResult ProcessText()
        {
            
            // --- Create an API Request String ---
            //Retrieve the API Key from a secret location (can only be used for public Google stuff)
            string key = System.Web.Configuration.WebConfigurationManager.AppSettings["LUISKey"];
            string appID = System.Web.Configuration.WebConfigurationManager.AppSettings["LUISAppID"];

            //Get the user's text input 
            string textInput = Request.QueryString["q"];

            //Generate an API Request URL string
            string url = "https://westus2.api.cognitive.microsoft.com/luis/v2.0/apps/" + appID + "?subscription-key=" + 
                key + "&verbose=true&timezoneOffset=-480&q=" + textInput;
                
            // --- Send the request and get a response from LUIS API Server ---
            // Send a request to LUIS
            WebRequest request = WebRequest.Create(url);
            // Get the response from LUIS
            WebResponse response = request.GetResponse();
            // Start the data stream
            Stream stream = response.GetResponseStream();

            // --- Parse the reader string into a JSON Object that we can use ---
            // initialize the serializer and get a new stream reader.
            // Serializes to RootObject in the LUIS Response class.
            var serializer = new System.Web.Script.Serialization.JavaScriptSerializer()
                                  .Deserialize<LUISRootObject>(new StreamReader(stream)
                                  .ReadToEnd());

            // --- Clean up the API Request Connections ---
            stream.Close();
            response.Close();

            return Json(serializer, JsonRequestBehavior.AllowGet);
        }


        //Language processing method to give Remi a sense of humor
        //Uses HttpWebrequest to send a request and receive a response
        //resource: https://msdn.microsoft.com/en-us/library/system.net.httpwebrequest.accept(v=vs.110).aspx
        //Uses the same logic as 'ProcessText' to reader and parse the response
        public JsonResult TellJoke()
        {
            // --- Send the request and get a response from Google API Server ---
            //Set the url to the joke API
            string url = "https://icanhazdadjoke.com/";
            //Create a http web request object.
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            //Set the 'Accept' property to accept a Json object
            request.Accept = "application/json";
            //Set the 'User-Agent' Property as requested by the API owner: https://icanhazdadjoke.com/api
            request.UserAgent = "My Library (https://bitbucket.org/devonsmith/senior-project), My E-mail (reviAItest@gmail.com)";
            //The response from our request
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            //Set up the Stream to read in the response
            Stream stream = response.GetResponseStream();
            //read in the JSON response as a string
            string reader = new StreamReader(stream).ReadToEnd();

            // --- Parse the reader string into a JSON Object that we can use ---
            //initialize the serializer
            var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            //use the serializer to parse the 'reader' string into a JSON Object
            var joke = serializer.DeserializeObject(reader);

            return Json(joke, JsonRequestBehavior.AllowGet);
        }

    }
}