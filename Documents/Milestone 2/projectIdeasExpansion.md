Conversational Interface for Web

A web interface that provides access to many web APIs all in one place that can act as a personal assistant. Using voice identification, the software can determine who is talking to it and get their relevant information. As a web-based application it is accessible from anywhere on any device. This will allow for seamless integration into the user’s everyday life.

•	What is new/original about this idea? What are related websites/apps? (Be able to answer the question: isn’t somebody already doing this?)
		o	It is accessible anywhere that has a web browser. It can also authenticate users based on their voice print. Many conversational AIs have been built but they are all differently approached solutions (for instance, only being accessible in one location or having very limited “smart home” type functionality).

•	Why is this idea worth doing? Why is it useful and not boring?
		o	It is a new take on the personal assistant/conversational AI in that it will both be available anywhere and will have to be able to recognize users’ unique voices.

•	What resources will be required for you to complete this project that are not already included in the class. i.e. you already have the Microsoft stack, server, database so what else would you need? Additional API’s, frameworks or platforms you’ll need to use.
		o	Voice recognition API(s) (i.e., Microsoft Cognitive Services) along with potentially many other APIs (for instance, Calendar APIs to access scheduling information). 

•	What algorithmic content is there in this project? i.e. what algorithm(s) will you have to develop or implement in order to do something central to your project idea? (Remember, this isn’t just a software engineering course, it is your CS degree capstone course!)
		o	We will have to program the intent behavior based on the string received by the language recognition API.
		
•	Rate the topic with a difficulty rating of 1-10. One being supremely easy to implement (not necessarily short though). Ten would require the best CS students using lots of what they learned in their CS degree, plus additional independent learning, to complete successfully.
		o	8
		
-------------------------------------------------------------------------------------------
		
Merged Conversation Engine with AI Moderation

A tool for streamers to merge all chat streams into a singular application. This would both feed chats from multiple locations into one stream, and allow the streamer to communicate simultaneously to all the same channels. For further enhancement, allowing the streamer to stream to multiple channels within the same program and implementing machine learning based moderation tools.
	
•	What is new/original about this idea? What are related websites/apps? (Be able to answer the question: isn’t somebody already doing this?)
		o	It hasn't been done before. Some programs allow the streamers to stream to multiple platforms, however they do not allow for chat aggregation.

•	Why is this idea worth doing? Why is it useful and not boring?
		o	It reduces frustration when streaming as well as increasing viewer interest and engagement.

•	What resources will be required for you to complete this project that are not already included in the class. i.e. you already have the Microsoft stack, server, database so what else would you need? Additional API’s, frameworks or platforms you’ll need to use.
		o	The APIs for the various streaming platforms (Twitch, YouTube, Mixer, etc) as well as the content moderation API from Microsoft Cognitive Services.

•	What algorithmic content is there in this project? i.e. what algorithm(s) will you have to develop or implement in order to do something central to your project idea? (Remember, this isn’t just a software engineering course, it is your CS degree capstone course!)
		o	Connection translation, format changing; a lot of algorithm calculations to interconnect the various chat streams.

•	Rate the topic with a difficulty rating of 1-10. One being supremely easy to implement (not necessarily short though). Ten would require the best CS students using lots of what they learned in their CS degree, plus additional independent learning, to complete successfully.
		o	6